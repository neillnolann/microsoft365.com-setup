[Microsoft365.com/setup](https://micro-soft-365setup.com/) 

Set up Microsoft 365 through [Microsoft365.com/setup](https://micro-soft-365setup.com/)   and use Microsoft apps such as Word, PowerPoint, and Excel, online
storage, cloud-connected features, and more. If you have a Microsoft account associated with Outlook, OneDrive, Xbox Live, 
or Skype, you can proceed with [Microsoft365.com/setup](https://micro-soft-365setup.com/)  login.
